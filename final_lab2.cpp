
#include <vector>
#include <stack>
#include <queue>
using namespace std;
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <string>
#include <algorithm>
#include<string>
vector<int> randvals;
bool verbose = false;
int len = 0;
//########################################################################
class Event
{
public:
    int timestamp;
    int created;
    int Epid;
    int transition;
    Event(int, int, int, int);//Constructor declared
    
    //Not using previous state and new state, since the transition will model that
};


Event :: Event(int ts, int c, int pid, int t) //Defined
{
    timestamp = ts;
    created = c;
    Epid = pid;
    transition = t;
}


/////////////////////////////////////////////////////////////////

class Process
{
public:
    int pid;
    int arrivalTime;
    int totalCPUTime;
    int CPUBurst;
    int IOBurst;
    int prev_time;
    int ft;
    int tt;
    int it; //Time in blocked state
    int cw; //Time in ready state - waiting to get to running
    int rem_cb; //rem bursts - rem
    int random_cb; //
    int spr; //static priority
    int dpr; //dynamic priority
    int rbt;
    int rrbt;
    Process(int, int, int, int, int);
    Process();
};

Process :: Process(int ppid, int at, int ct, int cb, int ib)
{
    pid = ppid;
    arrivalTime = at;
    rem_cb = totalCPUTime = ct;
    CPUBurst = cb;
    IOBurst = ib;
    ft = tt = it = cw = 0;
    prev_time =random_cb= 0;
    dpr = spr = 0;
    rbt = rrbt = 0;
    
}

Process :: Process()
{
    pid = arrivalTime = totalCPUTime = CPUBurst = IOBurst =random_cb= ft = tt =rem_cb= it = cw = prev_time=dpr=spr= 0;
    rbt = rrbt = 0;
}

//############################################################

class Scheduler
{
public:
    vector <Process> ReadyQueue;
    int type;
    // vector <vector <Process>> Expired;
    //vector <vector <Process>> Active;
    int quantum;
    //public:
    vector<Process> a1;
    vector<Process> a2;
    vector<Process> a3;
    vector<Process> a0;
    
    vector<Process> e1;
    vector<Process> e2;
    vector<Process> e3;
    vector<Process> e0;
    vector<Process> Expired;
    virtual Process get_next_process() = 0;
    virtual void add_process(Process) = 0;
    virtual void add_process2(Process) = 0;
    
};

//////////////////////////////////////////////////////////

class FCFS : public Scheduler
{
    
public:
    FCFS(int);
    void add_process(Process);
    Process get_next_process();
    void add_process2(Process);
};

FCFS::FCFS(int t)
{
    type = t;
    //quantum = q;
}

void FCFS::add_process2(Process P)
{
    
}

void FCFS::add_process(Process p)
{
    ReadyQueue.push_back(p);
}

Process FCFS::get_next_process()
{
    Process pp;
    //pp.pid = -1; //will behave as null
    if(!ReadyQueue.empty())
    {
        pp = ReadyQueue.front();
        //Process p = ReadyQueue.front();
        ReadyQueue.erase(ReadyQueue.begin());
        //return p;
    }
    return pp;
}

/////////////////////////////////////////////////////////////////

class LCFS : public Scheduler
{
public:
    LCFS(int);
    void add_process(Process);
    Process get_next_process();
    void add_process2(Process);
    
};

LCFS::LCFS(int t)
{
    type = t;
}

void LCFS::add_process(Process p)
{
    ReadyQueue.push_back(p);
}
void LCFS::add_process2(Process p)
{
    
}
Process LCFS::get_next_process()
{
    //Return the last one
    Process P;
    if(ReadyQueue.size()!=0)
    {
        P = ReadyQueue[ReadyQueue.size()-1]; //Get last process
        //P = ReadyQueue.back();
        ReadyQueue.pop_back();
    }
    return P;
}

//////////////////////////////////////////////////////////////////

class SJF : public Scheduler
{
public:
    SJF(int);
    void add_process(Process);
    Process get_next_process();
    void add_process2(Process);
};

SJF :: SJF(int t)
{
    type = t;
}

void SJF :: add_process2(Process P)
{
    
}

void SJF :: add_process(Process P)
{
    //The shortest job will be decided according to the CPUBurst time initially and subsequently for the remaining CPU burst time.
    int pos = ReadyQueue.size();
    for (int i = 0; i < ReadyQueue.size(); ++i) {
        if (P.rem_cb < ReadyQueue[i].rem_cb) {
            pos = i;
            break;
        }
    }
    ReadyQueue.insert(ReadyQueue.begin() + pos, P);
    
}

Process SJF::get_next_process()
{
    Process P;
    if(ReadyQueue.size()!=0)
    {
        P = ReadyQueue.front();
        ReadyQueue.erase(ReadyQueue.begin());
    }
    return P;
}
//////////////////////////////////////////////////////////////////

class RR:public Scheduler
{
    //int quantum;
public:
    RR(int);
    RR(int, int);
    void add_process(Process);
    Process get_next_process();
    void add_process2(Process);
};

RR::RR(int t)
{
    type = t;
    //quantum = q;
}
RR::RR(int t, int q)
{
    type = t;
    quantum = q;
}
void RR::add_process2(Process)
{
    
}

void RR::add_process(Process P)
{
    /*
     //Do insertion sort in ReadyQueue wrt to
     int loc = ReadyQueue.size();
     for (int i = 0; i < ReadyQueue.size(); ++i) {
     if (P.arrivalTime < ReadyQueue[i].arrivalTime) {
     loc = i;
     break;
     }
     }
     ReadyQueue.insert(ReadyQueue.begin() + loc, P);
     */
    //Dont do insertion in ReadyQ; it will be handled by doing insertion sort in event queue.
    
    ReadyQueue.push_back(P);
}

Process RR::get_next_process()
{
    Process P;
    if(ReadyQueue.size()!=0)
    {
        P = ReadyQueue.front();
        ReadyQueue.erase(ReadyQueue.begin());
    }
    return P;
}

///////////////////////////////////////////////////////////////////

class PRIO: public Scheduler
{
    public :
    PRIO(int, int);
    //vector <Event> Active;
    
    void add_process(Process P);
    Process get_next_process();
    void add_process2(Process P);
    Process get_next_process2();
};

PRIO::PRIO(int t, int q)
{
    type = 5;
    quantum = q;
}

void PRIO::add_process(Process P)
{
    if(P.dpr==3)
        a3.push_back(P);
    else if(P.dpr==2)
        a2.push_back(P);
    else if(P.dpr==1)
        a1.push_back(P);
    else if(P.dpr==0)
        a0.push_back(P);
    
    
}
Process PRIO::get_next_process()
{
    Process P;
    if(a1.size()==0 && a2.size()==0 && a3.size()==0 && a0.size()==0)
    {
        a3.swap(e3);
        a2.swap(e2);
        a1.swap(e1);
        a0.swap(e0);
        
    }
    
    if(a3.size()!=0)
    {
        P = a3.front();
        a3.erase(a3.begin());
    }
    else if(a2.size()!=0)
    {
        P = a2.front();
        a2.erase(a2.begin());
    }
    else if(a1.size()!=0)
    {
        P = a1.front();
        a1.erase(a1.begin());
    }
    else if(a0.size()!=0)
    {
        P = a0.front();
        a0.erase(a0.begin());
    }
    
    return P;
}

void PRIO::add_process2(Process P)
{
    if(P.dpr==3)
        e3.push_back(P);
    else if(P.dpr==2)
        e2.push_back(P);
    else if(P.dpr==1)
        e1.push_back(P);
    else if(P.dpr==0)
        e0.push_back(P);
}
////////////////////////////////////////////////////////////////////
//Defining Queues here
vector<Event> Event_Queue;

//////////////////////////////////////////////////////////////////
int isNum(string str) {
    int n = 0;
    for (int i = 0; i < str.size(); ++i)
    {
        if (!isdigit(str[i]))
        {
            n = -1;
            break;
        }
        else
        {
            n = n * 10 + str[i] - '0';
        }
    }
    return n;
}
///////////////////////////////////////////////////////////////////

Scheduler* getScheduler(int Type) {
    //std::vector<Process> proc = this->getProcesses();
    switch (Type) {
        case 1: return new FCFS(1);
        case 2: return new LCFS(2);
        case 3: return new SJF(3);
    }
}

Scheduler* getScheduler(int t, int q){
    switch(t){
        case 4: return new RR(4, q);
        case 5: return new PRIO(5, q);
    }
}


////////////////////////////////////////////////////////////////
void put_event(Event e)
{
    int pos = Event_Queue.size();

    for (int i = 0; i < Event_Queue.size(); ++i)
    {
        if (e.timestamp < Event_Queue[i].timestamp)
        {
            pos = i;
            break;
        }
    }
    Event_Queue.insert(Event_Queue.begin() + pos, e);
    //Keeping events in sorted order of their timestamps
}

Event get_event()
{
    Event e(0, 0, 0, 0);
    if (!Event_Queue.empty())
    {
        e = Event_Queue.front();
        Event_Queue.erase(Event_Queue.begin());
    }
    return e;
}



///////////////////////////////////////////////////////////////

typedef struct IO_interval {
    int beg;
    int end;
} IO_interval;

int ofs = 0;
int myrandom(int burst)
{
    int k = 1 + randvals[ofs] % burst;
    ofs++;
    if(ofs == randvals.size()-1)
        ofs = 0;
    return  k;
    
}
vector<Process> Expired;

void add_to_expired(Process P)
{
    //Insertion sort
    int pos = Expired.size();

    for (int i = 0; i < Expired.size(); ++i) {
        if (P.dpr > Expired[i].dpr) {
            pos = i;
            break;
        }
    }
    Expired.insert(Expired.begin() + pos, P);
    
}

std::vector<IO_interval>  ioWait;
void io_insert(IO_interval obj)
{
    int pos = ioWait.size();
    
    for (int i = 0; i < ioWait.size(); ++i) {
        if (obj.beg < ioWait[i].beg) {
            pos = i;
            break;
        }
    }
    ioWait.insert(ioWait.begin() + pos, obj);
    
}

int mergeIntervals(vector<IO_interval>& intervals)
{   int x = 0;
    // Test if the given set has at least one interval
    if (intervals.size() <= 0)
        return 0;
    
    // Create an empty stack of intervals
    stack<IO_interval> s;
    
    // sort the intervals based on start time
    //sort(intervals.begin(), intervals.end(), compareInterval);
    
    // push the first interval to stack
    s.push(intervals[0]);
    
    // Start from the next interval and merge if necessary
    for (int i = 1 ; i < intervals.size(); i++)
    {
        // get interval from stack top
        IO_interval top = s.top();
        
        // if current interval is not overlapping with stack top,
        // push it to the stack
        if (top.end < intervals[i].beg)
        {
            s.push( intervals[i] );
        }
        // Otherwise update the ending time of top if ending of current
        // interval is more
        else if (top.end < intervals[i].end)
        {
            top.end = intervals[i].end;
            s.pop();
            s.push(top);
        }
    }
    
    
    while (!s.empty())
    {
        IO_interval t = s.top();
        x = x + t.end - t.beg;
        s.pop();
    }
    
    return x;
}

///////////////////////////////////////////////////////////////
int main(int argc, char* argv[])
{
    //getting input in any order
    //Event_Queue
    Scheduler* scheduler;
    ifstream dataInput;
    ifstream randInput;
    double cpu_util = 0.0;
    double ioUtilization = 0.0;
    int    totTime = 0;
    double turnaround = 0.0;
    double CWTime = 0.0;
    vector<Process> process_vector;
    int type = 0;
    int quantum = 0;
    int rrbt = 0; //Remaining random burst time, Random burst time
    int rbt = 0;
    //int input_file;
    //int rand;
    
    bool flag = true;
    verbose = false;
    for(int i = 1; i<argc; i++)
    {
    
       if(argv[i][0] == '-')
        {
            if(argv[i][1]=='s'){
                if(argv[i][2] == 'F')
                    type = 1; //FCFS
                else if(argv[i][2] == 'L')
                    type = 2; //LCFS
                else if(argv[i][2] == 'S')
                    type = 3; //SJF
                else if(argv[i][2] == 'R')
                {
                    type = 4;
                    string temp(argv[i]);
                    //string quant = temp.substr(3);//Start from index 3
                    
                    string tt;
                    for(int ii = 3; ii<temp.length(); ii++)
                        tt = tt + temp.at(ii);
                    
                    //quantum = isNum(quant);
                    quantum = isNum(tt);
                }
                else if(argv[i][2] == 'P')
                {
                    type = 5;
                    string temp(argv[i]);
                    string quant = temp.substr(3);//Start from index 3
                    quantum = isNum(quant);
                }
            }
            
            
            
        }//Did this to get type of scheduler and quantum where applicable
        
        else
        {
            if(flag == true) //input file
            {
                //ifstream file(argv[i]);
                dataInput.open(argv[i]);
                flag = false;
            }
            else  //rand file
            {
                //ifstream file(argv[i]);
                randInput.open(argv[i]);
                
            }
            //Now to get the input file
            
        }
    }
    
    /////////////////////////////////////////////////////////////////////////////////////////////////
    //Now open the file
    
    if(!dataInput.is_open()) {
        std::cout << "\n\nThe file could not be opened.  Program execution is now terminating.";
        return 10;
    }
    
    if(!randInput.is_open()) {
        std::cout << "\n\nThe file could not be opened.  Program execution is now terminating.";
        return 10;
    }
    
    string rd;
    randInput>>rd;
    len = isNum(rd);
    
    while(!randInput.eof())
    {
        randInput >> rd;
        randvals.push_back(isNum(rd));
    }
    
    //Reading from the random file.
    //////////////////////////////////////////////////////////////////////////
    //You opened the file, congrats. Now get the processes.
    int pid_count = 0;
    //Create a Process vector
    
    //create a dummy var
    string dummy;
    while(!dataInput.eof())
    {
        dataInput >> dummy;
        int a1 = isNum(dummy);
        dataInput >> dummy;
        int a2 = isNum(dummy);
        dataInput >> dummy;
        int a3 = isNum(dummy);
        dataInput >> dummy;
        int a4 = isNum(dummy);
        
        Process p(pid_count, a1, a2, a3, a4);
        pid_count = pid_count + 1;
        //p.spr = myrandom(4);
        //p.dpr = myrandom(p.spr) - 1;
        process_vector.push_back(p);
        //Inserting process into this static Process_vector; Will add entries of this
        //into the Event_Queue - The initial transition will be 0 (just created)
        
    }
    
    if(type==4||type==5)
        scheduler = getScheduler(type, quantum);
    else
        scheduler = getScheduler(type);
    
    process_vector.pop_back(); //Potentially hazardous
    for(int i = 0; i<process_vector.size(); i++)
    {
        Event e(process_vector[i].arrivalTime, process_vector[i].arrivalTime, process_vector[i].pid, 1);
        put_event(e);
        process_vector[i].spr = myrandom(4);
        process_vector[i].dpr = process_vector[i].spr-1;//(rand() % (int)(process_vector[i].spr));
        //output = min + (rand() % (int)(max - min + 1))
        
    }
    //cout << process_vector.size()<<endl;
    
    //#################################################################
    int current_time = 0;
    current_time = process_vector[0].arrivalTime; //Keeps track of current time.
    Event current_event(0,0,0,0); //To be used in while loop
    //Now extract from Event queue
    int nxtProStart = 0;
    int ib = 0;
    int cb = 0;
    
    while(!Event_Queue.empty())
    {
        // if((scheduler->type == 5) && (scheduler->ReadyQueue.size()==0))
        //{
        //  scheduler->ReadyQueue = scheduler->Expired;
        //scheduler->Expired.clear();
        //}
                
        
            current_event = get_event();
        
        //current_time = max(current_time, current_event.timestamp);
        //current_time = current_time > current_event.timestamp ? current_time : current_event.timestamp; //Deviation1
        if(current_time < current_event.timestamp)
            current_time = current_event.timestamp;
        // cout << current_time<<endl;
        //updating time here
        int c_pid = current_event.Epid;
        //Have indexed process_vector by the pid
        if(current_event.transition == 1)
        {
            //Put process in ready queue.
            process_vector[c_pid].prev_time = current_time;
            scheduler->add_process(process_vector[c_pid]);
            
            //Since current time will change
            Event e(process_vector[c_pid].arrivalTime, process_vector[c_pid].arrivalTime, c_pid, 2);
            put_event(e);
        }
        else if (current_event.transition == 2) {
            //Ready to running
            // if there is still a process running
            //Since only 1 process is run at a time
            if (current_event.timestamp < nxtProStart) {
                current_event.timestamp = nxtProStart;
                put_event(current_event);
                continue;
            }
            
            
            
            
            Process p = scheduler->get_next_process();
            
            
            //Getting single process here since only 1 process in Running
            //Only in Ready to running you get process from ready queue
            c_pid = p.pid;
            current_event.created = process_vector[c_pid].prev_time;
            process_vector[c_pid].cw = (process_vector[c_pid].cw + current_time - current_event.created);
            //Total cpu wait time.
            
            
            
            
            
            if(scheduler->type == 4||scheduler->type==5)
            {
                if(process_vector[c_pid].rbt == 0)
                    process_vector[c_pid].rbt = myrandom(process_vector[c_pid].CPUBurst);
                //Randomly calculating CPU burst
                
                if(quantum > process_vector[c_pid].rbt)
                {
                    /*
                     if quantum > remaining burst time
                     1) Check if Q is <= remaining cpu time
                     - if its lesser, then the smallest is rbt. rbt is quantum in this case. Go to io state
                     2) else if(Q>remaining cpu time)
                     in this instance quantum will be remaining cpu time(not burst time)
                     go to finished state
                     
                     */
                    
                    if(process_vector[c_pid].rbt < process_vector[c_pid].rem_cb)
                    {
                        //You run for quantum time and since Q>rbt, rbt is the smallest here.
                        Event e(current_time+process_vector[c_pid].rbt, current_time, c_pid, 3);//io state
                        nxtProStart = current_time + process_vector[c_pid].rbt;
                        process_vector[c_pid].rem_cb -= process_vector[c_pid].rbt;
                        process_vector[c_pid].rbt = 0;
                        
                        put_event(e);
                    }
                    else //if(quantum > process_vector[c_pid].rem_cb)
                    {
                        //Q>remaining cpu time; thus the maximum amt of units you can complete is rem_cb
                        Event e(current_time+process_vector[c_pid].rem_cb, current_time, c_pid, 6);//finished state
                        nxtProStart = current_time + process_vector[c_pid].rem_cb;
                        process_vector[c_pid].rem_cb  = 0;
                        process_vector[c_pid].rbt = 0;
                        put_event(e);
                        
                    }
                    
                }
                else if(quantum <= process_vector[c_pid].rbt)
                {
                    //quantum <= remaining burst time
                    if(quantum >= process_vector[c_pid].rem_cb)
                    {
                        
                        
                        Event e(current_time+process_vector[c_pid].rem_cb, current_time, c_pid, 6);//Finished state
                        nxtProStart = current_time + process_vector[c_pid].rem_cb;
                        process_vector[c_pid].rem_cb =0 ;
                        process_vector[c_pid].rbt = 0;
                        put_event(e);
                        
                    }
                    else
                    {
                        if(process_vector[c_pid].rbt == quantum)
                        {
                            Event e(current_time+process_vector[c_pid].rbt, current_time, c_pid, 3);//block state
                            nxtProStart = current_time + process_vector[c_pid].rbt;
                            process_vector[c_pid].rem_cb -= process_vector[c_pid].rbt;
                            process_vector[c_pid].rbt = 0;
                            put_event(e);
                        }
                        else
                        {
                            Event e(current_time+quantum, current_time, c_pid, 5);//prempt state
                            nxtProStart = current_time + quantum;
                            process_vector[c_pid].rem_cb -= quantum;
                            process_vector[c_pid].rbt -= quantum;
                            put_event(e);
                        }
                    }
                }
                
            }
            
            else
            {
                ///FCFS Scheduler - No preemption - Not RR or Prio
                int cbr = myrandom(process_vector[c_pid].CPUBurst);
                //Need to implement getRandomNumber()
                process_vector[c_pid].random_cb = cbr;
                
                if(process_vector[c_pid].rem_cb<=cbr)
                {
                    int cbrr = process_vector[c_pid].rem_cb;
                    process_vector[c_pid].rem_cb = 0; //setting remaining to 0, and put it in finishes state
                    Event e(current_time + cbrr, current_time, c_pid, 6); //Done
                    put_event(e);
                    nxtProStart = current_time + cbrr;
                }
                else //if(cbr < process_vector[c_pid].rem_cb)
                {
                    process_vector[c_pid].rem_cb-=cbr; //Reduce rem time
                    Event e(current_time + cbr, current_time, c_pid, 3);
                    put_event(e);
                    //Go in blocked - but arent you done? Shouldnt you go to state 6
                    //No you gotta finish total burst time
                    nxtProStart = current_time + cbr;
                    
                }
            }
        }
        
        
        else if(current_event.transition == 3)
        {   //Running to blocked
            int ib =  myrandom(process_vector[c_pid].IOBurst);
            
            //Wait in blocked state
            Event e(current_time + ib, current_time, c_pid, 4);
            process_vector[c_pid].it+=ib; //keeping track of total time process spends in blocked state
            put_event(e);
            IO_interval io;
            io.beg = current_time;
            io.end = current_time + ib;
            io_insert(io); //inserting in sorted order
        }
        
        else if(current_event.transition == 4)
        {
            //Block to Ready
            //No delay in transition from Block to Ready
            process_vector[c_pid].dpr = process_vector[c_pid].spr-1;
            process_vector[c_pid].prev_time = current_time;
            scheduler->add_process(process_vector[c_pid]);
            Event e(current_time, current_time, c_pid, 2);
            put_event(e);
        }
        else if(current_event.transition == 5)
        {    //Running -> Ready
            process_vector[c_pid].prev_time = current_time;
            Event e(current_time, current_time, c_pid, 2);
            
            if(scheduler->type==5)
            {
                process_vector[c_pid].dpr--; // After quantum burst over I decrement priority
                if(process_vector[c_pid].dpr==-1)
                {
                    process_vector[c_pid].dpr = process_vector[c_pid].spr -1;
                    scheduler->add_process2(process_vector[c_pid]);
                    put_event(e);
                }//If dpr = -1, add process to expired Queue but DONT add to active queue.
                else
                {
                    put_event(e);
                    scheduler->add_process(process_vector[c_pid]);
                }
            }
            else
            {
                put_event(e);
                scheduler->add_process(process_vector[c_pid]);
            }
            
        }
        else if(current_event.transition == 6)
        {
            process_vector[c_pid].ft = current_time;  //Process finished. The current time will be finishing time. Used to calculate turnaround time
            process_vector[c_pid].tt = process_vector[c_pid].ft - process_vector[c_pid].arrivalTime;
            // cout << current_time<<endl;
        }
    }
    
    
        
    ioUtilization = mergeIntervals(ioWait);
    
    if(scheduler->type==1)
        cout << "FCFS";
    else if(scheduler->type==2)
        cout << "LCFS";
    else if (scheduler -> type == 3)
        cout << "SJF";
    else if (scheduler -> type == 4)
        cout << "RR";
    else if (scheduler -> type == 5)
        cout << "PRIO";
    
    if (scheduler->type == 4 || scheduler->type==5)
       std::cout << " " << scheduler->quantum;
    
    printf("\n");
    
    for (int i = 0; i < process_vector.size(); ++i)
    {
        
        printf("%04d: %4d %4d %4d %4d %d |  %4d  %4d  %4d  %4d\n", process_vector[i].pid, process_vector[i].arrivalTime, process_vector[i].totalCPUTime, process_vector[i].CPUBurst, process_vector[i].IOBurst, process_vector[i].spr,  process_vector[i].ft, process_vector[i].tt, process_vector[i].it, process_vector[i].cw);
        if (process_vector[i].ft > totTime)
           totTime = process_vector[i].ft;
        
        cpu_util += process_vector[i].totalCPUTime;
        turnaround += process_vector[i].tt;
        CWTime += process_vector[i].cw;

    }
    printf("SUM: %d %.2lf %.2lf %.2lf %.2lf %.3lf\n", totTime, (cpu_util*100)/totTime, (ioUtilization*100)/totTime, turnaround/process_vector.size(), CWTime/process_vector.size(), ((double)process_vector.size()*100)/totTime);
    
    //cout<<ofs;
    return 0;
    
}













# Scheduler
# Scheduler2
